package exception;

public class MaxDepositException extends Exception{

	public MaxDepositException() {
		System.out.println("Maximum deposit amount is 100000");
	}
}
