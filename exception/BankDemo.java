package exception;

public class BankDemo {

	public static void main(String[] args) throws MinWithdrowalException, MaxWithdrowalException{
		
		Account account = new Account("12345");
		System.out.println("Depositing 200 LKR");
		
		try {
			account.deposit(1000000);
			account.withdrowal(500);
			
			System.out.println(account.getAccNumber() + "-->" + account.getBalance());
		}catch(MinDepositException | MaxDepositException | MinWithdrowalException | MaxWithdrowalException e) {
			e.printStackTrace();
		}
	}
}
